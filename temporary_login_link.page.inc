<?php

/**
 * @file
 * Contains temporary_login_link.page.inc.
 *
 * Page callback for Temporary login link entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Temporary login link templates.
 *
 * Default template: temporary_login_link.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_temporary_login_link(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
