<?php
/**
 * @file
 * Contains \Drupal\temporary_login_links\Routing\RouteSubscriber.
 *
 * This route subscriber should restrict access to the Drupal original login
 * form if user decided to (at module configuration page).
 */

namespace Drupal\temporary_login_links\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\temporary_login_links\Form\TemporaryLoginLinkConfigurationForm;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\temporary_login_links\Routing
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('user.login')) {
      // Get "Actions on original login form" configuration form selected value.
      $drupal_origin_login_form_action
        = \Drupal::config('temporary_login_links_configuration_form.settings')->get('original_login_form_options');

      // And restrict access to the Drupal original login form if selected.
      if ($drupal_origin_login_form_action === TemporaryLoginLinkConfigurationForm::LOGIN_FORM_RESTRICT_ACCESS) {
        $requirements = [
          '_access' => 'FALSE',
        ];
        $route->setRequirements($requirements);
      }

    }
  }

}
