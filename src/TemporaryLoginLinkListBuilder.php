<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\TemporaryLoginLinkListBuilder.
 */

namespace Drupal\temporary_login_links;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;
use Drupal\user\Entity\User;

/**
 * Defines a class to build a listing of Temporary login link entities.
 *
 * @ingroup temporary_login_links
 */
class TemporaryLoginLinkListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_query = \Drupal::service('entity.query')->get('temporary_login_link');
    $entity_query->condition('status', NODE_PUBLISHED);
    $entity_query->sort('status', 'DESC');
    $entity_query->sort('created', 'DESC');
    $ids = $entity_query->execute();

    return $this->storage->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['user'] = $this->t('Target user');
    $header['status'] = $this->t('Status');
    $header['link'] = $this->t('Link');
    $header['created'] = $this->t('Created');
    $header['disable'] = $this->t('Disable');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\temporary_login_links\Entity\TemporaryLoginLink */
    $user = User::load($entity->get('target_uid')->getValue()[0]['target_id']);
    $link_active = $entity->get('status')->getValue()[0]['value'] == 1;

    $row['user'] = $user->getAccountName();
    $row['status'] = $link_active ? t('active') : t('used');

    $access_track_url = new Url(
      'temporary_login_links.temporary_login_controller_accessLoginForm',
      ['hash' => $entity->get('access_hash')->getValue()[0]['value']],
      ['absolute' => TRUE]
    );

    $row['link'] = $link_active ? $access_track_url : '-';

    $row['created'] = date('H:i d.m.y', $entity->getCreatedTime());

    $row['disable'] = $link_active ? $this->l($this->t('Disable'), new Url(
      'temporary_login_links.temporary_login_controller_invalidateAccessLink',
      ['hash' => $entity->get('access_hash')->getValue()[0]['value']]
    )) : '-';

    return $row + parent::buildRow($entity);
  }

}
