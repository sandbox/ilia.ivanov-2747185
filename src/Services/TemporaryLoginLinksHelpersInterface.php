<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\TemporaryLoginLinksHelpersInterface.
 */

namespace Drupal\temporary_login_links\Services;

use Drupal\temporary_login_links\TemporaryLoginLinkInterface;

/**
 * Interface TemporaryLoginLinksHelpersInterface.
 *
 * @package Drupal\temporary_login_links
 */
interface TemporaryLoginLinksHelpersInterface {

  /**
   * Validate if single access is expired or not.
   *
   * @param \Drupal\temporary_login_links\TemporaryLoginLinkInterface $entity
   *   Temporary login link entity.
   *
   * @return bool
   *   does link expired or not
   */
  public function isLinkExpired(TemporaryLoginLinkInterface $entity);

  /**
   * Generate access hash string for user.
   *
   * @param int $target_id
   *   Target user id.
   *
   * @return bool|string hash or FALSE if user not found.
   *   hash or FALSE if user not found.
   *
   * @internal param string $salt Salt to be added to the links hash.*   Salt to be added to the links hash.
   */
  public function generateAccessHash($target_id);

  /**
   * Get temporary login entities.
   *
   * Load temporary login entities with passed properties.
   *
   * @param array $properties
   *   Properties to load by.
   *
   * @return \Drupal\temporary_login_links\Entity\TemporaryLoginLink[]
   *   array of loaded entities
   */
  public function getAccessTrack(array $properties);

}
