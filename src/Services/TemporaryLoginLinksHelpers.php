<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\TemporaryLoginLinksHelpers.
 */

namespace Drupal\temporary_login_links\Services;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\temporary_login_links\Event\LoginLinkExpiredEvent;
use Drupal\temporary_login_links\TemporaryLoginLinkInterface;

/**
 * Class TemporaryLoginLinksHelpers.
 *
 * @package Drupal\temporary_login_links
 */
class TemporaryLoginLinksHelpers implements TemporaryLoginLinksHelpersInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher definition.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $event_dispatcher;

  /**
   * TemporaryLoginLinksHelpers constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
   *   Event dispatcher.
   */
  public function __construct(EntityTypeManager $entity_type_manager, ContainerAwareEventDispatcher $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->event_dispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function isLinkExpired(TemporaryLoginLinkInterface $entity) {
    // Get temporary link expiration limit or use 900 seconds as default.
    $expiration_limit
      = \Drupal::config('temporary_login_links_configuration_form.settings')->get('expiration_limit')?: 900;

    // Time diff in minuted.
    $expired = (time() - $entity->getCreatedTime()) >= $expiration_limit;

    if ($expired) {
      $this->event_dispatcher
        ->dispatch(
          LoginLinkExpiredEvent::NAME,
          new LoginLinkExpiredEvent($entity->getHash())
        );
    }

    return $expired;
  }

  /**
   * {@inheritdoc}
   */
  public function generateAccessHash($target_id) {
    /* @var \Drupal\user\UserInterface $user */
    if ($user = $this->entityTypeManager->getStorage('user')->load($target_id)) {
      return user_pass_rehash($user, REQUEST_TIME);
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessTrack(array $properties) {
    return $this->entityTypeManager
      ->getStorage('temporary_login_link')
      ->loadByProperties($properties);
  }

}
