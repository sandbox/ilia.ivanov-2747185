<?php
/**
 * @file
 * Contains TemporaryLoginAccessValidation class.
 *
 * A custom validation callback for the user login form.
 */

namespace Drupal\temporary_login_links\Validation;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;

/**
 * Class TemporaryLoginAccessValidation.
 */
class TemporaryLoginAccessValidation {

  /**
   * Validate if super admin user trying to login via Drupal regular login form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function checkSuperAdminLoggingIn($form, FormStateInterface $form_state) {
    if ($form_state->getFormObject()->getFormId() === 'user_login_form') {
      // Find user by entered name and check if it's a super admin user (id: 1).
      $username = $form_state->getValue('name');
      $user
        = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['name' => $username]);

      // Statement's second part is a bit paranoid, but just in case lets check
      // that found object id too.
      if ($user && (!isset($user[1]) || $user[1]->id() != 1)) {
        $form_state->setErrorByName('name', t('You are not allowed to login!'));
      }
    }
  }

  /**
   * Validate if user trying to login with own link or not.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function checkTemporaryLinkUserAccess($form, FormStateInterface $form_state) {
    if (\Drupal::request()->get(RouteObjectInterface::ROUTE_NAME) === 'temporary_login_links.temporary_login_controller_accessLoginForm') {
      /** @var \Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpersInterface $temporary_login_service */
      $temporary_login_service = \Drupal::service('temporary_login_links.helpers');
      $temporary_link_entity = $temporary_login_service->getAccessTrack([
        'access_hash' => \Drupal::request()->get('hash'),
        'status' => NODE_PUBLISHED,
      ]);
      $temporary_link_entity = count($temporary_link_entity) == 1 ? $temporary_link_entity[key($temporary_link_entity)] : FALSE;
      if ($temporary_link_entity) {
        $user = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['name' => $form_state->getValue('name')]);
        $user = count($user) == 1 ? $user[key($user)] : FALSE;
        if ($user
          && $user->id() != $temporary_link_entity->get('target_uid')->target_id
        ) {
          $form_state->setErrorByName('name', t('You are not allowed to access this link!'));
        }
      }
      else {
        $form_state->setErrorByName('name', t('Invalid hash passed!'));
      }
    }
  }

}
