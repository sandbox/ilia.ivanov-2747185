<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\LoginLinkEventSubscriber.
 */

namespace Drupal\temporary_login_links\EventSubscriber;

use Drupal\temporary_login_links\Event\LoginLinkExpiredEvent;
use Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpersInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class LoginLinkEventSubscriber.
 *
 * @package Drupal\temporary_login_links
 */
class LoginLinkEventSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\temporary_login_links\Controller\TemporaryLoginLinksHelpersInterface definition.
   *
   * @var \Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpersInterface
   */
  private $temporary_login_service;

  /**
   * LoginLinkEventSubscriber constructor.
   *
   * @param \Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpersInterface $temporary_login_service
   *   Temporary login links service.
   */
  public function __construct(TemporaryLoginLinksHelpersInterface $temporary_login_service) {
    $this->temporary_login_service = $temporary_login_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LoginLinkExpiredEvent::NAME][] = array('invalidateLink', 100);

    return $events;
  }

  /**
   * invalidateLink subscriber.
   *
   * Will invalidate temporary access link.
   *
   * @param \Drupal\temporary_login_links\Event\LoginLinkExpiredEvent $event
   *   Event object.
   */
  public function invalidateLink(LoginLinkExpiredEvent $event) {
    $temporary_access_links = $this->temporary_login_service->getAccessTrack(
      ['access_hash' => $event->getHash(), 'status' => NODE_PUBLISHED]
    );

    if ($temporary_access_links) {
      // Should be only one event, but just in case...
      /* @var \Drupal\temporary_login_links\Entity\TemporaryLoginLink[] $temporary_access_links  */
      foreach ($temporary_access_links as $temporary_access_link) {
        $temporary_access_link->setPublished(NODE_NOT_PUBLISHED);
        $temporary_access_link->save();
      }
    }
  }

}
