<?php
/**
 * @file
 * Contains LoginLinkExpiredEvent class.
 */

namespace Drupal\temporary_login_links\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class LoginLinkExpiredEvent.
 */
class LoginLinkExpiredEvent extends Event {

  const NAME = 'temporary_login_links.login_link_expired';

  /**
   * Hash value.
   *
   * @var string
   */
  protected $hash;

  /**
   * {@inheritdoc}
   */
  public function __construct($hash) {
    $this->hash = $hash;
  }

  /**
   * Get hash.
   *
   * @return string
   *   hash
   */
  public function getHash() {
    return $this->hash;
  }

}
