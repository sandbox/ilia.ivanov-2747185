<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\Controller\TemporaryLoginController.
 */

namespace Drupal\temporary_login_links\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\temporary_login_links\Event\LoginLinkExpiredEvent;
use Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpersInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;

/**
 * Class TemporaryLoginController.
 *
 * @package Drupal\temporary_login_links\Controller
 */
class TemporaryLoginController extends ControllerBase {

  /**
   * ContainerAwareEventDispatcher definition.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * TemporaryLoginLinksHelpersInterface definition.
   *
   * @var \Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpersInterface
   */
  private $temporaryLoginService;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpersInterface $temporary_login_service
   *   Temporary login links service.
   */
  public function __construct(
    ContainerAwareEventDispatcher $event_dispatcher,
    TemporaryLoginLinksHelpersInterface $temporary_login_service
  ) {
    $this->eventDispatcher = $event_dispatcher;
    $this->temporaryLoginService = $temporary_login_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('temporary_login_links.helpers')
    );
  }

  /**
   * Access regular Drupal login form.
   *
   * @param string $hash
   *   Access hash value.
   *
   * @return string
   *   redirect response or rendered login form page
   */
  public function accessLoginForm($hash) {
    $user = $this->currentUser();
    $temporary_link_entity = $this->temporaryLoginService->getAccessTrack(['access_hash' => $hash, 'status' => NODE_PUBLISHED]);
    $temporary_link_entity = count($temporary_link_entity) == 1 ? $temporary_link_entity[key($temporary_link_entity)] : FALSE;
    /* @var \Drupal\temporary_login_links\Entity\TemporaryLoginLink $temporary_link_entity */
    if ($temporary_link_entity) {
      if (!$user->isAuthenticated()) {
        if ($this->temporaryLoginService->isLinkExpired($temporary_link_entity)) {
          $message = $this->t(
            'Login link expired! Please contact an administrator: %email!',
            ['%email' => $this->config('system.site')->get('mail')]
          );
        }
        else {
          // TODO: should find some better way to do it.
          $_SESSION['temporary_login_links']['access_track_hash'] = $hash;

          return $this->formBuilder()->getForm('Drupal\user\Form\UserLoginForm', 'temporary_login_form');
        }
      }
      else {
        $message = $this->t('Allowed only for anonymous users!');
      }
    }
    else {
      $message = $this->t(
        'Invalid temporary access link! Please contact an administrator: %email!',
        ['%email' => $this->config('system.site')->get('mail')]
      );
    }

    drupal_set_message($message, 'warning');
    return $this->redirect('<front>');
  }

  /**
   * Create temporary access link.
   *
   * Link to directly create temporary access link entity.
   *
   * @param int $target_uid
   *   Target user id.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   redirect response with
   */
  public function createTemporaryAccessLink($target_uid) {
    if (is_numeric($target_uid) && User::load($target_uid)) {
      if ($this->temporaryLoginService->getAccessTrack(['target_uid' => $target_uid, 'status' => NODE_PUBLISHED])) {
        drupal_set_message($this->t('Temporary access link already generated for this user!'), 'error');
      }
      else {
        $this->entityTypeManager()
          ->getStorage('temporary_login_link')
          ->create([
            'target_uid' => $target_uid,
            'name' => 'Temporary link ' . $target_uid,
            'status' => NODE_PUBLISHED,
          ])
          ->save();
      }

      return $this->redirect('entity.user.edit_form', ['user' => $target_uid]);
    }
    else {
      drupal_set_message($this->t('Invalid user passed!'));
      return $this->redirect('<front>');
    }
  }

  /**
   * Invalidate temporary access link.
   *
   * @param string $hash
   *   Link hash to be invalidated.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the temporary login links collection page.
   */
  public function invalidateAccessLink($hash) {
    $this->eventDispatcher
      ->dispatch(
        LoginLinkExpiredEvent::NAME,
        new LoginLinkExpiredEvent($hash)
      );

    return $this->redirect('entity.temporary_login_link.collection');
  }

}
