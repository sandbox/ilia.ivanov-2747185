<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\Form\TemporaryLoginLinkDeleteForm.
 */

namespace Drupal\temporary_login_links\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Temporary login link entities.
 *
 * @ingroup temporary_login_links
 */
class TemporaryLoginLinkDeleteForm extends ContentEntityDeleteForm {

}
