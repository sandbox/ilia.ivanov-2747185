<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\Form\TemporaryLoginLinkForm.
 */

namespace Drupal\temporary_login_links\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpersInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Temporary login link edit forms.
 *
 * @ingroup temporary_login_links
 */
class TemporaryLoginLinkForm extends ContentEntityForm {

  /**
   * TemporaryLoginLinksHelpersInterface definition.
   *
   * @var \Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpersInterface
   */
  private $temporaryLoginService;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpersInterface $temporary_login_service
   *   Temporary login links service.
   */
  public function __construct(EntityManagerInterface $entity_manager, TemporaryLoginLinksHelpersInterface $temporary_login_service) {
    parent::__construct($entity_manager);
    $this->temporaryLoginService = $temporary_login_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('temporary_login_links.helpers')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Temporary login link.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Temporary login link.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.temporary_login_link.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $entity = $form_state->getFormObject()->getEntity();
    if ($entity->isNew()
      && $this->temporaryLoginService
        ->getAccessTrack(['target_uid' => $form_state->getValue('target_uid')[0]['target_id'], 'status' => NODE_PUBLISHED])
    ) {
      $form_state->setErrorByName('target_uid', t('Temporary access link already generated for this user!'));
    }
  }

}
