<?php
/**
 * @file
 * Contains TemporaryLoginLinkConfigurationForm.
 */

namespace Drupal\temporary_login_links\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TemporaryLoginLinkConfigurationForm.
 */
class TemporaryLoginLinkConfigurationForm extends ConfigFormBase {

  // Actions on original login form available at module configuration form.
  const LOGIN_FORM_RESTRICT_ACCESS    = 'login_form_restrict_access';
  const LOGIN_FORM_ONLY_ADMIN_ACCESS  = 'login_form_only_admin_access';
  const LOGIN_FORM_DO_NOTHING_ACCESS  = 'login_form_do_nothing_access';

  /**
   * Router builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routerBuilder;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $router_builder
   *   Router builder service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteBuilderInterface $router_builder) {
    parent::__construct($config_factory);

    $this->routerBuilder = $router_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'temporary_login_links_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'temporary_login_links_configuration_form.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Default settings.
    $config = $this->config('temporary_login_links_configuration_form.settings');

    $form['expiration_limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expiration limit'),
      '#description' => $this->t('Link expiration time value in seconds (default: 15 * 60 sec).'),
      '#default_value' => $config->get('expiration_limit'),
      '#required' => TRUE,
      '#size' => 10,
      '#maxlength' => 10,
    ];

    $form['original_login_form_options'] = [
      '#type' => 'radios',
      '#title' => $this->t('Actions on original login form'),
      '#default_value' => $config->get('original_login_form_options')?:self::LOGIN_FORM_DO_NOTHING_ACCESS,
      '#options' => [
        self::LOGIN_FORM_RESTRICT_ACCESS    => $this->t('Hide login form completely (WARNING: only "drush uli" access will be availiable)'),
        self::LOGIN_FORM_ONLY_ADMIN_ACCESS  => $this->t('Only user 1 can login'),
        self::LOGIN_FORM_DO_NOTHING_ACCESS  => $this->t('Do nothing, I\'ll code something myself'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_numeric($form_state->getValue('expiration_limit'))) {
      $form_state->setErrorByName('expiration_limit', $this->t('Invalid expiration limit value passed!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('temporary_login_links_configuration_form.settings');
    $config->set('expiration_limit', $form_state->getValue('expiration_limit'));
    $config->set('original_login_form_options', $form_state->getValue('original_login_form_options'));
    $config->save();

    // Rebuild router in order Drupal original form visibility was changed.
    // This is important for the RouteSubscriber where access to this original
    // login form might be altered.
    $this->routerBuilder->rebuild();

    parent::submitForm($form, $form_state);
  }

}
