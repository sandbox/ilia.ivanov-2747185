<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\TemporaryLoginLinkInterface.
 */

namespace Drupal\temporary_login_links;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface for defining Temporary login link entities.
 *
 * @ingroup temporary_login_links
 */
interface TemporaryLoginLinkInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Temporary login link name.
   *
   * @return string
   *   Name of the Temporary login link.
   */
  public function getName();

  /**
   * Sets the Temporary login link name.
   *
   * @param string $name
   *   The Temporary login link name.
   *
   * @return \Drupal\temporary_login_links\TemporaryLoginLinkInterface
   *   The called Temporary login link entity.
   */
  public function setName($name);

  /**
   * Gets the Temporary login link creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Temporary login link.
   */
  public function getCreatedTime();

  /**
   * Sets the Temporary login link creation timestamp.
   *
   * @param int $timestamp
   *   The Temporary login link creation timestamp.
   *
   * @return \Drupal\temporary_login_links\TemporaryLoginLinkInterface
   *   The called Temporary login link entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the entity target's user entity.
   *
   * @return \Drupal\user\UserInterface
   *   The target user entity.
   */
  public function getTargetUser();

  /**
   * Returns the entity target's user ID.
   *
   * @return int|null
   *   The target user ID, or NULL in case the target user ID field has not been
   *   set on the entity.
   */
  public function getTargetUserId();

  /**
   * Sets the entity target's user ID.
   *
   * @param int $target_id
   *   The target user id.
   *
   * @return $this
   */
  public function setTargetUserId($target_id);

  /**
   * Sets the entity target's user entity.
   *
   * @param \Drupal\user\UserInterface $account
   *   The target user entity.
   *
   * @return $this
   */
  public function setTargetUser(UserInterface $account);

  /**
   * Returns the Temporary login link published status indicator.
   *
   * Unpublished Temporary login link are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Temporary login link is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Temporary login link.
   *
   * @param bool $published
   *   TRUE to set this Temporary login link to published, FALSE to set it to
   *   unpublished.
   *
   * @return \Drupal\temporary_login_links\TemporaryLoginLinkInterface
   *   The called Temporary login link entity.
   */
  public function setPublished($published);

  /**
   * Sets the Temporary login link access hash.
   *
   * @param string $hash
   *   String hash value.
   *
   * @return \Drupal\temporary_login_links\TemporaryLoginLinkInterface
   *   The called Temporary login link entity.
   */
  public function setHash($hash);

  /**
   * Gets the Temporary login link access hash.
   *
   * @return string
   *   hash
   */
  public function getHash();

}
