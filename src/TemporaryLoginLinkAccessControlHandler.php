<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\TemporaryLoginLinkAccessControlHandler.
 */

namespace Drupal\temporary_login_links;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Temporary login link entity.
 *
 * @see \Drupal\temporary_login_links\Entity\TemporaryLoginLink.
 */
class TemporaryLoginLinkAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\temporary_login_links\TemporaryLoginLinkInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished temporary login link entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published temporary login link entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit temporary login link entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete temporary login link entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add temporary login link entities');
  }

}
