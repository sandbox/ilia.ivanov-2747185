<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\Entity\TemporaryLoginLink.
 */

namespace Drupal\temporary_login_links\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Temporary login link entities.
 */
class TemporaryLoginLinkViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['temporary_login_link']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Temporary login link'),
      'help' => $this->t('The Temporary login link ID.'),
    );

    return $data;
  }

}
