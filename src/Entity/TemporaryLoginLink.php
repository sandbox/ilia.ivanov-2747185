<?php

/**
 * @file
 * Contains \Drupal\temporary_login_links\Entity\TemporaryLoginLink.
 */

namespace Drupal\temporary_login_links\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\temporary_login_links\TemporaryLoginLinkInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Temporary login link entity.
 *
 * @ingroup temporary_login_links
 *
 * @ContentEntityType(
 *   id = "temporary_login_link",
 *   label = @Translation("Temporary login link"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\temporary_login_links\TemporaryLoginLinkListBuilder",
 *     "views_data" = "Drupal\temporary_login_links\Entity\TemporaryLoginLinkViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\temporary_login_links\Form\TemporaryLoginLinkForm",
 *       "add" = "Drupal\temporary_login_links\Form\TemporaryLoginLinkForm",
 *       "edit" = "Drupal\temporary_login_links\Form\TemporaryLoginLinkForm",
 *       "delete" = "Drupal\temporary_login_links\Form\TemporaryLoginLinkDeleteForm",
 *     },
 *     "access" = "Drupal\temporary_login_links\TemporaryLoginLinkAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\temporary_login_links\TemporaryLoginLinkHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "temporary_login_link",
 *   admin_permission = "administer temporary login link entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "tuid" = "target_uid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/temporary-login-link/{temporary_login_link}",
 *     "add-form" = "/admin/structure/temporary-login-link/add",
 *     "edit-form" = "/admin/structure/temporary-login-link/{temporary_login_link}/edit",
 *     "delete-form" = "/admin/structure/temporary-login-link/{temporary_login_link}/delete",
 *     "collection" = "/admin/structure/temporary-login-link",
 *   },
 *   field_ui_base_route = "temporary_login_link.settings"
 * )
 */
class TemporaryLoginLink extends ContentEntityBase implements TemporaryLoginLinkInterface {
  use EntityChangedTrait;
  
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);

    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetUser() {
    return $this->get('target_uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetUserId() {
    return $this->get('target_uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetUserId($target_id) {
    $this->set('target_uid', $target_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetUser(UserInterface $account) {
    $this->set('target_uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setHash($hash) {
    $this->set('access_hash', $hash);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHash() {
    return $this->get('access_hash')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Temporary login link entity.'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Temporary login link entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Created by'))
      ->setDescription(t('The user ID of author of the Temporary login link entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['target_uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Targeted to'))
      ->setDescription(t('The user ID of target user of the Temporary login link entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Temporary login link entity.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['access_hash'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Access hash'))
      ->setDescription(t('This hash will be added to the URL in order to access login form'))
      ->setRequired(TRUE)
      ->setSettings(array(
        'max_length' => 100,
        'text_processing' => 0,
      ));

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Temporary login link is used or not.'))
      ->setDefaultValue(NODE_PUBLISHED);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Temporary login link entity.'))
      ->setDisplayOptions('form', array(
        'type' => 'language_select',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    if ($this->isNew()) {
      /* @var \Drupal\temporary_login_links\Services\TemporaryLoginLinksHelpers $temporary_login_links_service */
      $temporary_login_links_service = \Drupal::service('temporary_login_links.helpers');
      if ($hash = $temporary_login_links_service->generateAccessHash($this->get('target_uid')
        ->getValue()[0]['target_id'])
      ) {
        $this->setHash($hash);
        parent::preSave($storage);
      }
      else {
        throw new \InvalidArgumentException('Target user is not defined!');
      }
    }
  }

}
