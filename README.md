# Temporary login links   

Imagine you suppose to disable the regular Drupal username / password login on your site for some reason (in order to replace it with some other kind way of authentication). At the same time you required to have kind of a backdoor on the site where you might have a access to site via username / password login from.   

#This module allows you to  

* generate unique login links for the users in order to provide access to the username / password login form
* via *'User roles allowed to access temporary login links'* permission can configure which roles are allowed to access temporary login links
* configure temporary login links expiration time limit
* configure actions on the "original" Drupal login form (disable access, allow only user 1 to login, do nothing)

Links expiration time are configurable on the admin config page:   
`/admin/config/development/temporary-login-links`   

# Installation guide

1. Download module and place it to the module folder   
2. Install module though the UI or in the command line using drush   
3. Navigate to the configuration page `/admin/config/development/temporary-login-links` and setup the expiration limit in second (otherwise default 15 min value will be used) and actions on the "original" Drupal login form 
4. Navigate to the user profile page `user/XXX/edit` and generate a one-time login link by pressing the "Create temporary login link for user" link   
5. Generated login link will be displayed on the profile page   

# P.S.   

- All active links are available on the temporary login links list page `/admin/structure/temporary-login-link`   
- Entity settings page available here `/admin/structure/temporary-login-link/settings`
